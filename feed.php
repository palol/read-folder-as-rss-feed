<?php
header('Content-type: text/xml; charset=utf-8');
/*
	Runs from a directory containing files to provide an
	RSS 2.0 feed that contains the list and modification times for all the
	files ordered by reversed alphabetical order.
*/
$feedName = "CV version history";
$feedDesc = "Feed for the versions of the CVs";
$feedLocale = "en-GB";
$feedTtl = 10080;
$protocol = $_SERVER['SERVER_PORT'] === '80' ? 'http://' : 'https://';
$feedURI = $protocol . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$feedBaseURI = dirname($feedURI) . '/'; // must end in trailing forward slash (/).
$allowed_ext = ".pdf, .PDF";
$author = "Luca Palo";
$author_email = "palo@programmer.net";
$license = "Copyright (c) 2002 - ". date("Y") .", " . $author;
$generator = "PHP list of the content of current folder as RSS 2.0.1 Atom feed.";

echo '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
	<title>'.$feedName.'</title>
	<link>'.$feedURI.'</link>
	<description>'.$feedDesc.'</description>
	<language>'.$feedLocale.'</language>
	<copyright>'.$license.'</copyright>
	<generator>'.$generator.'</generator>
	<docs>http://www.rssboard.org/rss-specification</docs>
	<ttl>'.$feedTtl.'</ttl>
	<managingEditor>'.$author_emai.'</managingEditor>
	<webMaster>'.$author_email.'</webMaster>
';

$files = array();
$names = array();
$order = array();
$dir = opendir("./");

while(($file = readdir($dir)) !== false)
{
	$path_info = pathinfo($file);
	$ext = $path_info['extension'];
	if($file !== '.' && $file !== '..' && !is_dir($file) && strpos($allowed_ext, $ext)>0)
	{
		$names[] = $file;
		$files[] = ['name' => $file, 'timestamp' => filectime($file)];
	}
}
closedir($dir);

natcasesort($names);//- we will use dates and times to sort the list.
$reversed_names = array_reverse($names, true);
reset($reversed_names);

for($i=0; $i<count($reversed_names);$i++) {
	$order[] = key($reversed_names);
	next($reversed_names);
}

$items_in_page = 7;
$start = isset( $_GET['start'] )?$_GET['start']:0;
$to = (($start + $items_in_page) < count($files)) ? ($start + $items_in_page) : count($files);

for($i=$start; $i<$to; $i++) {
		$position = $order[$i];
		echo "	<item>\n";
		echo "		<title>". $files[$position]['name']."</title>\n";
		echo "		<link>". $feedBaseURI . $files[$position]['name'] . "</link>\n";
		echo "		<description>Version ". date("D M j G:i:s T Y", $files[$position]['timestamp']) ." of the CV of Luca Palo</description>\n";
		echo "		<guid>urn:uuid:" . $files[$position]['name'] . "</guid>\n";
		echo "		<pubDate>" . $files[$position]['timestamp'] ."</pubDate>\n";
		echo "		<author>".$author."</author>\n";
		echo "   	</item>\n";
}

echo '
</channel>
</rss>';
