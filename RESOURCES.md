# USEFUL LINKS AND RESOURCES

- [Create an RSS feed with PHP, contents fetched from DB](https://www.carronmedia.com/create-an-rss-feed-with-php/)
- [Write RSS feed from folder file list](https://gist.github.com/vsoch/4898025919365bf23b6f)
- [Allowing redirect to index.php in subfolders](https://stackoverflow.com/questions/54806695/403-forbidden-you-dont-have-permission-to-access-in-the-laravel)
- Standard .htaccess file already defines, that folders (and files) are loaded if they exist. [.htaccess disable WordPress rewrite rules for folder and its contents](https://wordpress.stackexchange.com/questions/11149/htaccess-disable-wordpress-rewrite-rules-for-folder-and-its-contents)
- [How to Fix the 403 Forbidden Error in WordPress](https://www.wpbeginner.com/wp-tutorials/how-to-fix-the-403-forbidden-error-in-wordpress/)
- [How to tag an older commit in Git?](https://stackoverflow.com/questions/4404172/how-to-tag-an-older-commit-in-git)
- Change the message of an already committed git tag: `git tag <tag name> <tag name>^{} -f -m "New message."`
- [Media type .pdf](https://www.rfc-editor.org/rfc/rfc3778.txt)
- [The Atom Syndication Format](https://tools.ietf.org/html/rfc4287)
- [Get the key of natcasesort array](https://stackoverflow.com/questions/2348269/how-to-get-the-key-of-the-current-item-of-an-array)
